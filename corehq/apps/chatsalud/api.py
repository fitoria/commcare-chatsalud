from urllib import urlencode
from urllib2 import urlopen
from corehq.apps.sms.util import clean_phone_number
from corehq.apps.sms.mixin import SMSBackend
from couchdbkit.ext.django.schema import *
from corehq.apps.chatsalud.forms import CronosBackendForm
from django.conf import settings

from exceptions import CronosException


class CronosBackend(SMSBackend):
    is_global = True
    api_key = StringProperty()

    @classmethod
    def get_api_id(cls):
        return "CRONOS"

    @classmethod
    def get_generic_name(cls):
        return "cronos"

    @classmethod
    def get_template(cls):
        return "cronos/backend.html"

    @classmethod
    def get_form_class(cls):
        return CronosBackendForm

    def get_sms_interval(self):
        return 1

    def send(self, msg, delay=True, *args, **kwargs):
        phone_number = clean_phone_number(msg.phone_number)
        text = msg.text.encode("utf-8")
        token = kwargs.get("token")

        if token:
            raise CronosException("Token is required")

        #TODO probar acentos
        params = urlencode({
            "token" : token,
            "user_id": phone_number,
            "text" : text,
            "apikey": self.api_key
        })
        url = "http://api.cronos.mobi:8888/chat/salud/?%s" % params
        response = urlopen(url, timeout=settings.SMS_GATEWAY_TIMEOUT).read()
        return response
