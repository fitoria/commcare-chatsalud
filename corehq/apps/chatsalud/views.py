import json
from .api import CronosBackend
from corehq.apps.sms.api import incoming as incoming_sms
from django.http import HttpResponse, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt

EMPTY_RESPONSE = """<?xml version="1.0" encoding="UTF-8" ?>
<Response></Response>"""

@csrf_exempt
def sms_in(request):
    if request.method == "GET":
        token = request.GET.get("token")
        from_ = request.GET.get("user_id")
        body = request.GET.get("text")
        incoming_sms(
            from_,
            body,
            CronosBackend.get_api_id(),
            backend_message_id=token
        )
        return HttpResponse(EMPTY_RESPONSE)
    else:
        return HttpResponseBadRequest("GET Expected")
