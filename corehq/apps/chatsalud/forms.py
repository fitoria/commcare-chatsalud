from django.forms.fields import *
from django.utils.translation import ugettext_lazy as _
from corehq.apps.sms.forms import BackendForm
from dimagi.utils.django.fields import TrimmedCharField
from crispy_forms import layout as crispy


class CronosBackendForm(BackendForm):
    api_key = TrimmedCharField(
        label=_("Api Key")
    )

    @property
    def gateway_specific_fields(self):
        return crispy.Fieldset(
            _("Cronos Settings"),
            'api_key',
        )
